package ru.tsc.kirillov.tm.component;

import ru.tsc.kirillov.tm.api.controller.ICommandController;
import ru.tsc.kirillov.tm.api.controller.IProjectController;
import ru.tsc.kirillov.tm.api.controller.IProjectTaskController;
import ru.tsc.kirillov.tm.api.controller.ITaskController;
import ru.tsc.kirillov.tm.api.repository.ICommandRepository;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.service.*;
import ru.tsc.kirillov.tm.constant.ArgumentConst;
import ru.tsc.kirillov.tm.constant.TerminalConst;
import ru.tsc.kirillov.tm.controller.CommandController;
import ru.tsc.kirillov.tm.controller.ProjectController;
import ru.tsc.kirillov.tm.controller.ProjectTaskController;
import ru.tsc.kirillov.tm.controller.TaskController;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.kirillov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.repository.CommandRepository;
import ru.tsc.kirillov.tm.repository.ProjectRepository;
import ru.tsc.kirillov.tm.repository.TaskRepository;
import ru.tsc.kirillov.tm.service.*;
import ru.tsc.kirillov.tm.util.DateUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService =  new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    private void initLogger() {
        loggerService.info("** Добро пожаловать в Task Manager **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** Task Manager завершил свою работу **");
            }
        });
    }

    public void run(final String[] args) {
        if (processArgument(args))
            close();

        initData();
        initLogger();
        while (true) {
            try {
                System.out.println("\nВведите команду:");
                final String cmdText = TerminalUtil.nextLine();
                processCommand(cmdText);
                System.out.println("[Ок]");
                loggerService.command(cmdText);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[Ошибка]");
            }
        }
    }

    private void initData() {
        taskService.create("Тестовая задача", "Простая задача");
        taskService.create("Вторая задача", "Простая задача");
        taskService.create("Ещё одна задача", "Простая задача");

        projectService.create("Тестовый проект", "Простой проект");
        projectService.create("Второй проект", "Простой проект");
        projectService.create("Ещё один проект", "Простой проект");
        projectService.add(
                new Project(
                        "Тест с датой",
                        Status.IN_PROGRESS,
                        DateUtil.toDate("04.10.2019")
                )
        );
        projectService.add(
                new Project(
                        "Не запущенный тестовый проект",
                        Status.NOT_STARTED,
                        DateUtil.toDate("05.03.2018")
                )
        );
        projectService.add(
                new Project(
                        "Выполняющийся проект",
                        Status.IN_PROGRESS,
                        DateUtil.toDate("16.02.2020")
                )
        );
        projectService.add(
                new Project(
                        "Завершённый проект",
                        Status.COMPLETED,
                        DateUtil.toDate("22.01.2021")
                )
        );
    }

    private boolean processArgument(final String[] args) {
        if (args == null || args.length == 0)
            return false;

        final String firstArg = args[0];
        processArgument(firstArg);

        return true;
    }

    private void processCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty())
            throw new CommandNotSupportedException();
        switch (cmd) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;

            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskListByProjectId();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETED_BY_ID:
                taskController.completedTaskById();
                break;
            case TerminalConst.TASK_COMPLETED_BY_INDEX:
                taskController.completedTaskByIndex();
                break;

            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProject();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETED_BY_ID:
                projectController.completedProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETED_BY_INDEX:
                projectController.completedProjectByIndex();
                break;
            case TerminalConst.PROJECT_BIND_TASK_BY_ID:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.PROJECT_UNBIND_TASK_BY_ID:
                projectTaskController.unbindTaskToProject();
                break;

            default:
                throw new CommandNotSupportedException(cmd);
        }
    }

    private void processArgument(final String arg) {
        if (arg == null || arg.isEmpty())
            throw new ArgumentNotSupportedException();
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    private void close() {
        System.exit(0);
    }

}
