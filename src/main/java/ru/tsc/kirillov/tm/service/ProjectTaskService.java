package ru.tsc.kirillov.tm.service;

import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.service.IProjectTaskService;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kirillov.tm.exception.field.IdEmptyException;
import ru.tsc.kirillov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.kirillov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository,
                              final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    private void bindTaskToProject(final String projectId, final String taskId, boolean isAdd) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();

        task.setProjectId(isAdd ? projectId : null);
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        bindTaskToProject(projectId, taskId, true);
    }

    @Override
    public void unbindTaskToProject(final String projectId, final String taskId) {
        bindTaskToProject(projectId, taskId, false);
    }

    @Override
    public void removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty())
            throw new IdEmptyException();
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task: tasks)
            taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

}
