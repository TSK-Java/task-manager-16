package ru.tsc.kirillov.tm.util;

import ru.tsc.kirillov.tm.exception.system.value.ValueNotValidDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    String PATTERN = "dd.MM.yyyy";

    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    static Date toDate(final String value) {
        try {
            return FORMATTER.parse(value);
        } catch (final ParseException e) {
            ValueNotValidDate childException = new ValueNotValidDate(value, PATTERN);
            childException.initCause(e);
            throw childException;
        }
    }

    static String toString(final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }

}
