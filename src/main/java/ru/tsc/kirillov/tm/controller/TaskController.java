package ru.tsc.kirillov.tm.controller;

import ru.tsc.kirillov.tm.api.controller.ITaskController;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.util.DateUtil;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    private void printTask(final List<Task> tasks) {
        int idx = 0;
        for(final Task task: tasks) {
            if (task == null)
                continue;
            System.out.println(++idx + ". " + task);
        }
    }

    @Override
    public void showTaskList() {
        System.out.println("[Список задач]");
        System.out.println("Введите способ сортировки");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        printTask(taskService.findAll(sort));
    }

    @Override
    public void showTaskListByProjectId() {
        System.out.println("[Список задач проекта]");
        System.out.println("Введите ID проекта:");
        final String projectId = TerminalUtil.nextLine();
        printTask(taskService.findAllByProjectId(projectId));
    }

    @Override
    public void clearTasks() {
        System.out.println("[Очистка списка задач]");
        taskService.clear();
    }

    @Override
    public void createTask() {
        System.out.println("[Создание задачи]");
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        System.out.println("Введите дату начала:");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("Введите дату окончания:");
        final Date dateEnd = TerminalUtil.nextDate();
        taskService.create(name, description, dateBegin, dateEnd);
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[Удаление задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        taskService.removeByIndex(NumberUtil.fixIndex(index));
    }

    @Override
    public void removeTaskById() {
        System.out.println("[Удаление задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[Отображение задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = taskService.findOneByIndex(NumberUtil.fixIndex(index));
        showTask(task);
    }

    @Override
    public void showTaskById() {
        System.out.println("[Отображение задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        showTask(task);
    }

    private void showTask(Task task) {
        if (task == null)
            throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("Имя: " + task.getName());
        System.out.println("Описание: " + task.getDescription());
        System.out.println("Статус: " + Status.toName(task.getStatus()));
        System.out.println("Дата создания: " + DateUtil.toString(task.getCreated()));
        System.out.println("Дата начала: " + DateUtil.toString(task.getDateBegin()));
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[Обновление задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(NumberUtil.fixIndex(index), name, description);
    }

    @Override
    public void updateTaskById() {
        System.out.println("[Обновление задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[Изменение статуса задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Введите статус:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusById(id, status);
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[Изменение статуса задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        System.out.println("Введите статус:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusByIndex(NumberUtil.fixIndex(index), status);
    }

    @Override
    public void startTaskById() {
        System.out.println("Запуск задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[Запуск задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();

        taskService.changeTaskStatusByIndex(NumberUtil.fixIndex(index), Status.IN_PROGRESS);
    }

    @Override
    public void completedTaskById() {
        System.out.println("Завершение задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completedTaskByIndex() {
        System.out.println("[Завершение задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        taskService.changeTaskStatusByIndex(NumberUtil.fixIndex(index), Status.COMPLETED);
    }

}
