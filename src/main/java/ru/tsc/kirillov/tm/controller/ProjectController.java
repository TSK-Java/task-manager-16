package ru.tsc.kirillov.tm.controller;

import ru.tsc.kirillov.tm.api.controller.IProjectController;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.api.service.IProjectTaskService;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.DateUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;
import ru.tsc.kirillov.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showProjectList() {
        System.out.println("[Список проектов]");
        System.out.println("Введите способ сортировки");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = projectService.findAll(sort);
        int idx = 0;
        for(final Project project: projects) {
            if (project == null)
                continue;
            System.out.println(++idx + ". " + project);
        }
    }

    @Override
    public void clearProject() {
        System.out.println("[Очистка списка проектов]");
        projectService.clear();
    }

    @Override
    public void createProject() {
        System.out.println("[Создание проекта]");
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        System.out.println("Введите дату начала:");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("Введите дату окончания:");
        final Date dateEnd = TerminalUtil.nextDate();
        projectService.create(name, description, dateBegin, dateEnd);
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[Удаление проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = projectService.findOneByIndex(NumberUtil.fixIndex(index));
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void removeProjectById() {
        System.out.println("[Удаление проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null)
            throw new ProjectNotFoundException();
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[Отображение проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = projectService.findOneByIndex(NumberUtil.fixIndex(index));
        showProject(project);
    }

    @Override
    public void showProjectById() {
        System.out.println("[Отображение проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        showProject(project);
    }

    private void showProject(Project project) {
        if (project == null)
            throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("Имя: " + project.getName());
        System.out.println("Описание: " + project.getDescription());
        System.out.println("Статус: " + Status.toName(project.getStatus()));
        System.out.println("Дата создания: " + DateUtil.toString(project.getCreated()));
        System.out.println("Дата начала: " + DateUtil.toString(project.getDateBegin()));
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[Обновление проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(NumberUtil.fixIndex(index), name, description);
    }

    @Override
    public void updateProjectById() {
        System.out.println("[Обновление проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        projectService.updateById(id, name, description);
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[Изменение статуса проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Введите статус:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusById(id, status);
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[Изменение статуса проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        System.out.println("Введите статус:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusByIndex(NumberUtil.fixIndex(index), status);
    }

    @Override
    public void startProjectById() {
        System.out.println("Запуск проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[Запуск проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        projectService.changeProjectStatusByIndex(NumberUtil.fixIndex(index), Status.IN_PROGRESS);
    }

    @Override
    public void completedProjectById() {
        System.out.println("Завершение проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completedProjectByIndex() {
        System.out.println("[Завершение проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        projectService.changeProjectStatusByIndex(NumberUtil.fixIndex(index), Status.COMPLETED);
    }

}
