package ru.tsc.kirillov.tm.controller;

import ru.tsc.kirillov.tm.api.controller.ICommandController;
import ru.tsc.kirillov.tm.api.service.ICommandService;
import ru.tsc.kirillov.tm.constant.TerminalConst;
import ru.tsc.kirillov.tm.model.Command;
import ru.tsc.kirillov.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout() {
        showTerminalHeader(TerminalConst.ABOUT);
        System.out.println("Разработчик: Кириллов Максим");
        System.out.println("E-mail: mkirillov@tsconsulting.com");
    }

    @Override
    public void showHelp() {
        showTerminalHeader(TerminalConst.HELP);
        final Command[] commands = commandService.getCommands();
        for (final Command cmd: commands)
            System.out.println(cmd);
    }

    @Override
    public void showVersion() {
        showTerminalHeader(TerminalConst.VERSION);
        System.out.println("1.16.0");
    }

    @Override
    public void showUnexpectedCommand(final String cmd) {
        showTerminalHeader(TerminalConst.ERROR);
        System.err.printf("Команда `%s` не поддерживается.\n", cmd);
    }

    @Override
    public void showUnexpectedArgument(final String arg) {
        showTerminalHeader(TerminalConst.ERROR);
        System.err.printf("Аргумент `%s` не поддерживается.\n", arg);
    }

    public void showTerminalHeader(final String name) {
        System.out.printf("[%s]\n", name != null ? name.toUpperCase() : "N/A");
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "безлимитно" : NumberUtil.formatBytes(maxMemory);
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("Количество процесоров (ядер): " + availableProcessors);
        System.out.println("Свободно памяти: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Максимум памяти: " + maxMemoryFormat);
        System.out.println("Всего доступно памяти для JVM: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Используется памяти в JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getCommands();
        for (final Command cmd: commands) {
            final String name = cmd.getName();
            if (name == null || name.isEmpty())
                continue;
            System.out.println(cmd.getName());
        }
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getCommands();
        for (final Command cmd: commands) {
            final String arg = cmd.getArgument();
            if (arg == null || arg.isEmpty())
                continue;
            System.out.println(arg);
        }
    }

}
