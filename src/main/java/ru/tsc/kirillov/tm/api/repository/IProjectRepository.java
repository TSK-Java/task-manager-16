package ru.tsc.kirillov.tm.api.repository;

import ru.tsc.kirillov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    boolean existsById(String id);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project add(Project project);

    void clear();

    Integer count();

}
