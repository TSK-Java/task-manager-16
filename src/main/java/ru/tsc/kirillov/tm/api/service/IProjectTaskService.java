package ru.tsc.kirillov.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void unbindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

}
