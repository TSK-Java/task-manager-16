package ru.tsc.kirillov.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showHelp();

    void showVersion();

    void showUnexpectedCommand(String cmd);

    void showUnexpectedArgument(String arg);

    void showSystemInfo();

    void showCommands();

    void showArguments();

}
