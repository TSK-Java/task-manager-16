package ru.tsc.kirillov.tm.api.model;

import ru.tsc.kirillov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
